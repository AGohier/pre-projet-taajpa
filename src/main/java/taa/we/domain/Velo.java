package taa.we.domain;

import javax.persistence.Entity;
import javax.persistence.PrimaryKeyJoinColumn;

@Entity
@PrimaryKeyJoinColumn(name = "id")
public class Velo extends Sport {



    private String sun;
    private int temperature;
    private int win;

    public Velo() {
        super();
    }

    public String getSun() {
        return sun;
    }
    public void setSun(String sun) {
        this.sun = sun;
    }

    public int getTemperature() {
        return temperature;
    }
    public void setTemperature(int temperatur) {
        this.temperature = temperatur;
    }

    public int getWin() {
        return win;
    }
    public void setWin(int win) {
        this.win = win;
    }

    @Override
    public int evaluate(Lieu lieu) {
        // TODO request location info to rate activity regarding to activity attributes
        return super.evaluate(lieu);
    }
}
