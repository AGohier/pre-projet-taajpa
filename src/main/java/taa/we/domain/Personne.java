package taa.we.domain;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;

@Entity
public class Personne {
	
	
	private Long id;
	
	
	protected void setId(Long id) {
		this.id = id;
	}

	private String login; //doit être unique
	private String passwd;
	private String nom;

	private List<Sport> choixSports;
	
	private List<Lieu> lieuxFavoris;
	
	
	public Personne(String nom) {
		this.lieuxFavoris = new ArrayList<>();
		this.choixSports = new ArrayList<>();
		this.nom = nom;
	}
	
	public Personne() {}

	@Id
	@GeneratedValue
	public Long getId() {
		return id;
	}
	

	@ManyToMany
	public List<Lieu> getLieuxFavoris() {
		return lieuxFavoris;
	}

	public void setLieuxFavoris(List<Lieu> lieuxFavoris) {
		this.lieuxFavoris = lieuxFavoris;
	}
	
	@ManyToMany
	public List<Sport> getChoixSports() {
		return choixSports;
	}

	public void setChoixSports(List<Sport> choixSports) {
		this.choixSports = choixSports;
	}
	
	@Column(unique=true)
	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}
	
	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}


	public String getPasswd() {
		return passwd;
	}

	public void setPasswd(String passwd) {
		this.passwd = passwd;
	}

	
	
	public void addLieuxFavoris(Lieu lieu){
		if(lieu!=null && !this.lieuxFavoris.contains(lieu)){
			this.lieuxFavoris.add(lieu);
		}
	}
	
	public void removeLieuxFavoris(Lieu lieu){
		if(this.lieuxFavoris.contains(lieu)){
			this.lieuxFavoris.remove(lieu);
		}
	}
	
	public void addSport(Sport sport){
		if(sport!=null && !this.choixSports.contains(sport)){
			this.choixSports.add(sport);
		}
	}
	
	public void removeSport(Sport sport){
		if(this.choixSports.contains(sport)){
			this.choixSports.remove(sport);
		}
	}
	
	

}
