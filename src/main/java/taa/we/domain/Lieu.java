package taa.we.domain;


import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Lieu {
	
	@Id
	@GeneratedValue
	private Long id;
	private String nom;
	private String ville;
	private Integer codepostal;
	private Integer numdepartement;
	private double longitude;
	private double latitude;
	
	public Lieu() {}

	public Lieu(String nom, String ville, Integer codepostal, Integer numdepartement, double longitude,
			double latitude) {
		
		this.nom = nom;
		this.ville = ville;
		this.codepostal = codepostal;
		this.numdepartement = numdepartement;
		this.longitude = longitude;
		this.latitude = latitude;
	}
	
	
	public Long getId() {
		return id;
	}
	

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getVille() {
		return ville;
	}

	public void setVille(String ville) {
		this.ville = ville;
	}

	public Integer getCodepostal() {
		return codepostal;
	}

	public void setCodepostal(Integer codepostal) {
		this.codepostal = codepostal;
	}

	public Integer getNumdepartement() {
		return numdepartement;
	}

	public void setNumdepartement(Integer numdepartement) {
		this.numdepartement = numdepartement;
	}

	public double getLongitude() {
		return longitude;
	}

	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}

	public double getLatitude() {
		return latitude;
	}

	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}

}
