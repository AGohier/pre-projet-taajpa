package taa.we.domain;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.*;

@Entity
@Inheritance(strategy = InheritanceType.JOINED)
public class Sport {
	
	@Id
	@GeneratedValue
	private Long id;
	private String libelle;
	
	@ManyToMany(mappedBy="choixSports")
	private List<Personne> inscrits;
	
	@ManyToMany
	private List<Lieu> lieux;
	
	public Sport(String libelle) {
		this.lieux = new ArrayList<>();
		this.inscrits = new ArrayList<>();
		this.libelle = libelle;
	}
	
	public Sport() {}



	public void getId(Long id) {
		this.id = id;
	}

	
	public List<Personne> getInscrits() {
		return inscrits;
	}

	public void setInscrits(List<Personne> inscrits) {
		this.inscrits = inscrits;
	}
	
	
	public List<Lieu> getLieux() {
		return lieux;
	}

	public void setLieux(List<Lieu> lieux) {
		this.lieux = lieux;
	}
	
	public String getLibelle() {
		return libelle;
	}

	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}

	public void addLieu(Lieu lieu){
		
		if(lieu!=null  && !this.lieux.contains(lieu) ){
			this.lieux.add(lieu);
		}
		
	}
	
	public void addPersonne(Personne personne){
		
		if(personne!=null  && !this.inscrits.contains(personne) ){
			this.inscrits.add(personne);
		}
		
	}
	
	public void removeLieu(Lieu lieu){
		
		if(this.lieux.contains(lieu) ){
			this.lieux.remove(lieu);
		}
		
	}
	
	public void removePersonne(Personne personne){
		
		if(this.inscrits.contains(personne) ){
			this.inscrits.remove(personne);
		}
		
	}

	@Override
	public String toString() {
		
		String out = "Sport [id=" + id + ", nom=" + libelle + ", lieux = ";
		
		for (Lieu l : getLieux()){
			out +=  l.getNom()+ "("+l.getVille()+"), ";
		}
		
		return  out + "]";
	}
	
	public int evaluate (Lieu lieu) {
		return -1;
	}
	
}
