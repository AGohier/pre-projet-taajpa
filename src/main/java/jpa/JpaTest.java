package jpa;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

import taa.we.domain.Lieu;
import taa.we.domain.Sport;

public class JpaTest {
	
	
	private EntityManager manager;
	
	public JpaTest(EntityManager manager) {
		this.manager = manager;
	}


	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		EntityManagerFactory factory =   
	 			 Persistence.createEntityManagerFactory("dev");


		EntityManager manager = factory.createEntityManager();
		JpaTest test = new JpaTest(manager);

		
		EntityTransaction tx = manager.getTransaction();
		tx.begin();


		try {
			test.createSports();

		} catch (Exception e) {
			e.printStackTrace();
		}
		tx.commit();
		
		test.listSports();

		manager.close();
		EntityManagerHelper.closeEntityManagerFactory();
		//		factory.close();
	}
	
	
	private void createSports(){
		
		int numOfSports = manager.createQuery("Select s From Sport s", Sport.class).getResultList().size();
		
		if (numOfSports == 0) {
			Lieu lieu1 = new Lieu("Club Tennis", "RENNES", 35700, 35, 48.1399473, -1.6435799);
			Lieu lieu2 = new Lieu("NPO tennis", "NANTES", 44000, 44, 47.2313215, -1.6057888);
			manager.persist(lieu1);
			manager.persist(lieu2);
			
			Sport unsport = new Sport("TENNIS");
			unsport.addLieu(lieu1);
			unsport.addLieu(lieu2);

			manager.persist(unsport);
			

		}
		
	}
	
	
	private void listSports() {
		List<Sport> resultList = manager.createQuery("Select s From Sport s", Sport.class).getResultList();
		System.out.println("num of Sports:" + resultList.size());
		for (Sport next : resultList) {
			System.out.println("next sport: " + next);
		}
	}


}
